import bson
import pymongo
import pprint
import urllib.parse
import random
import numpy as np
import math
import time
from pymongo import mongo_client
from pymongo import cursor


username = urllib.parse.quote_plus('')	#Upisite svoj Username
password = urllib.parse.quote_plus('')	#Upisite svoju lozinku
client = mongo_client.MongoClient('mongodb://%s:%s@127.0.0.1' % (username, password))
db = client['websecradar']

#funkcija prolazi kroz sve kolekcije i vraca dictionary(ime_kolekcije: broj dokumenata)
#u dictionary ulaze samo kolekcije koje sadrze trazeni atribut

#attributes = lista atributa za pretragu
def getCollectionsForSearch(attributes):
    collection_dict = {}
    for coll_name in db.list_collection_names():
        if coll_name == "system.views":
            continue
        tmp_collection = db.get_collection(coll_name)
        number_of_docs = tmp_collection.estimated_document_count()
        doc = tmp_collection.find_one()
        if all(attribute in doc for attribute in attributes):
            collection_dict[coll_name] = number_of_docs

    collection_dict = dict(sorted(collection_dict.items(), key=lambda x:x[1], reverse=True))   
    return collection_dict

#generira pomocne podatke za funkciju getRandomId
#vraca razliku izmedu najveceg i najmanjeg id-a i najmanji id

#collection = ime kolekcije
def getDataForRandomId(collection):
    lowestId = db[collection].find().sort("_id", pymongo.ASCENDING).limit(1).next()["_id"]    #.generation_time
    highestId = db[collection].find().sort("_id", pymongo.DESCENDING).limit(1).next()["_id"]  #.generation_time
    lowest = int(str(lowestId)[0:8], 16) * 1000
    highest = int(str(highestId)[0:8], 16) * 1000
    dif = highest - lowest 
    return dif, lowest

#funkcija vraca slucajno odabrani id iz kolekcije

#collection = ime kolekcije
#random_num = pseudoslucajan broj
def getRandomId(collection, random_num): 
    dif, lowest = getDataForRandomId(collection)
    rnd = math.floor(math.floor(random_num * dif) / 1000) * 1000
    new_id = str(hex(int((lowest + rnd) / 1000))).ljust(26, "0")[2:]
    return new_id

#funkcija osigurava konzistentnost tako da dodaje dokumente iz ostalih
#kolekcija ako sadrže jednaku vrijednost za isti atribut

#other_cols = sve druge kolekcije koje sadrze trazene atribute OSIM kolekcije iz koje je dokument document
#document = slucajno dohvacen dokument
#attributes = lista atributa za pretragu
#doc_list = lista u koju se spremaju dohvaceni dokumenti
def addDocWithSameAttributeValue(other_cols, document, attributes, doc_list):
    query = dict()
    for attribute in attributes:
        query[attribute] = document[attribute]

    for col in other_cols:
        try:
            doc = db.get_collection(col).find_one(query).next()
            if doc not in doc_list:
                doc_list.append(doc)
        except:                                                     #ako ne postoji dokument koji se moze joinati s poslanim dokumentom
            continue

#funkcija vraca listu random dohvacenih Cursora

#collection_dict = dictionary(ime_kolekcije: broj dokumenata)
#doc_number = broj dokumenata koje funkcija treba vratiti
#seconds = broj sekundi nakon kojih se prekida potraga za ranodm dokumentima
#byte_size = vrati listu dokumenata nakon sto se premasi velicina u byteovima
#percentage = postotak baze koji treba dohvatiti
#attributes = lista atributa za pretragu
def generateRandomDocs(collection_dict, seed, doc_number, byte_size, percentage, attributes):
    #random.seed(seed)
    rng = np.random.default_rng(seed)
    doc_list = []
    total_doc_num = sum(collection_dict.values())

    if doc_number == 0:
        doc_number = math.ceil((percentage / 100) * total_doc_num)
    doc_range_list = [math.ceil(doc_number * (i / total_doc_num)) for i in collection_dict.values()]

    doc_range_list[0] = doc_range_list[0] - (len(doc_range_list) - 1)
    documents_size = 0

    for collection, document_count in collection_dict.items():
        j = 0
        other_cols = [x for x in collection_dict.keys() if x != collection]
        if document_count < 100000:
            for i in range(doc_range_list[j]):
                rnd = math.floor(rng.random() * document_count)
                document = db.get_collection(collection).find().skip(rnd).limit(1).next()
                doc_list.append(document)
                addDocWithSameAttributeValue(other_cols, document, attributes, doc_list) 
                documents_size += len(bson.BSON.encode(document))
                if byte_size != -1 and documents_size > byte_size:
                    print("Documents size = " + str(documents_size))
                    return doc_list
        else:
            for i in range(doc_range_list[j]):
                random_num = rng.random()
                _id = bson.ObjectId(getRandomId(collection, random_num))
                document = db.get_collection(collection).find({"_id": {"$gt": _id}}).next()
                doc_list.append(document)  
                addDocWithSameAttributeValue(other_cols, document, attributes, doc_list)       
        j += 1

    return doc_list



def main():
    print("Please enter attributes for search separated by space")
    attributes = input().split(" ")
    print("Enter 0 if you want to specify number of documents to retrieve, if you want to retrieve documents based on percentage of database enter 1")
    tmp = input()
    doc_num = 0
    percentage = 0
    if tmp == "0":
        print("Please enter the number of documents you want to retrieve")
        doc_num = input()
    else:
        print("Please enter percentage of documents from query you want to retrive")
        percentage = input()
    print("Please enter seed")
    seed = input()
    print("Please enter max size for list of documents (in bytes) - enter -1 if size is not important")
    bytes = input()

    collection_dict = getCollectionsForSearch(attributes)
    if len(collection_dict) == 0:
        print("The database does not contain combination of selected attributes")
    else:
        docs = generateRandomDocs(collection_dict, int(seed), int(doc_num), int(bytes), int(percentage), attributes)
        
        for doc in docs:
            pprint.pprint(doc)


if __name__ == "__main__":
    main()
